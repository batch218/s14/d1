console.log("Hello World!");
// enclose with quatation mark string dates
console.log("Hello World!");

console.
log(
	"Hello, everyone!"
)
//  ; delimeter
// we use delimeter to end our of code

// [Comments]
// - single line comments
// Shortcut: ctrl + /

// I am a single line comment


/* Multi-line comment*/
/* I am a multi line comment*/

/* shortcut: ctrl + shiift + / */

// Syntax and statement

// Statements in programming are instructions that we tell to compute to perform
// Syntax in programmin, it is set of rules that decribes how statements must be considered

// Variables
/*it is used to contain date
	- Syntax in declaring variables
	- let/const variableName	
*/
let myVariable = "Hello ";
// assignment operator (=);
console.log(myVariable);


// console.log(hello); will result to not defined error
/*
	Guides in writing variables:
	1. Use the 'let' keyword followed by the variable name of your chossing and use the assignment operator (=) to assign a value.
	2. Variable names should start with a lowecase character, use camelCase for multiple words.
	3. For constant variables, use the 'const' keyword.
	4. Varaible name should be indicative or descriptive of the word being stored.
	5. Never name a variable starting with numbers.
	6. Refrain from using space in declaring a variable.

*/
let productName = 'desktop computer' ;
console.log(productName);

let product = "Alvin's computer";
console.log(product)
let productPrice = 18999;
console.log(productPrice);

let interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
// variableName = newValue;
productName = "Laptop";
console.log(productName)

let friend = "kate";
friend= "Jane";
console.log(friend);

/*interest = 4.89;
console.log(interest);*/
 pi = 3.141618 ;
// pi = 3.15;
console.log(pi);

// Reassigning - a variable have a alue and re-assign a new one 
// Initialize - it is our first  a value

let supplier;// declaration
 supplier = "John Smith Trading"; // initializing
 console.log(supplier);

 supplier = "Zuitt Store";
 console.log(supplier);
 
 // Multiple variable declaration
let productCode = "DC017";
let productBrand = "Dell";
console.log(productCode, productBrand)

// Using a variable wiith a reserved keyword
//const let = "hello";
//console.log(let);

// [SECTION] Date Types

// Strings
// Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.
// String in Javascript is enclosed with single ('') or double ("") quote
let country = 'Philippines';
let province = 'Metro Manila'

console.log(province + ',' + country);
// We use + symbol to concatenant date / values
let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" +', ' +  "Metro Manila");

// Escape Characters (\)
// \n refers to creating a new line or set the text to new line;
console.log("line\nline2");

let mailAddress = "Metro Manila\n Philippines";
console.log(mailAddress);

let message = "John's employeee wnet home early."
console.log(message);
message = 'John\'s employeee went home early'
console.log(message)
// Numbers
let headcount = 26 ;
console.log(headcount);
// Decimal number / float / fraction
let grade = 98.7 ;
console.log(grade);

// Exponential Notation e for number of 0

let placeDistance = 2e2;
console.log(placeDistance);

console.log("John's grade last quarter is "+ grade);

// Arrays
// it is store multiple values with similar date type
let grades = [98.7, 95.4,90.2, 94.6];
// arrayName / elements
console.log(grades);

// Diffrent data types
// Storing different date types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);
// Objects
// Objectss are another special kind of date type that it used to mimic world objects/items.
// syentax:

/*
	:let/const objectName= {
		propertyA: value,
		propertyB; Value
	}
*/

let person = {
		fullName: "Juan Dela Cruz",
		// Key/ Property/ value
		Age:35,
		isMarried: false,
		/*console: ["0912 345 6789". "8123 7444"]
		addres:{
			houseNumber:"345"
			city: "Manila"
		}*/
	};
console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading:94.6

};
console.log(myGrades);

// type of operator
let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000,640,1000];
// myGrade as objects
// 'type of operator' - 
// we use type of operator to retrieve/know the data type.

 console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: number
console.log(typeof waterBills); //output: object
console.log(typeof myGrades); //output: object

// Constant Objects and Arrays
// We canot reassign the value of the variable, but we can change the element of the constant array. 
const anime = ["One Piece", "Fairy Tail", "Naruto","Demon Slayer", "Boruto","AOT"];
				//0 			1              2          3             4      5 
// index = is the positionof the element starting zero.
anime[4] = "Naruto";
console.log(anime);

// Null
// It is used to intentionally express the absence of a value
let spouse = null;
console.log(spouse);

// Undefined
// Represents the state of a variable that has been declared but without an assigned value
let fullName; //declaration
console.log(fullName);

